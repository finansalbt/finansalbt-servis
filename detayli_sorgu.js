var transactionIds = db.users.find({_id:ObjectId("5c0afac91eed450b6068b1c2")}).map(function(user) { 
  return user.transactions; 
});

db.transactions.aggregate(
   [
      { $match: { _id: { $in: transactionIds[0] } } },
      { $group : {
           _id : null,
           // TRY: { $sum: { $cond: [{$eq:["$currency_unit", "TRY"]}, { $multiply: ["$amount", {$cond: [{$eq:["$type", true]}, 1, -1]}] }, 0] } },
           // USD: { $sum: { $cond: [{$eq:["$currency_unit", "USD"]}, { $multiply: ["$amount", {$cond: [{$eq:["$type", true]}, 1, -1]}] }, 0] } },
           // EUR: { $sum: { $cond: [{$eq:["$currency_unit", "EUR"]}, { $multiply: ["$amount", {$cond: [{$eq:["$type", true]}, 1, -1]}] }, 0] } },
           
           
           TRY_income: { $sum: { $cond: [{$eq:["$currency_unit", "TRY"]}, { $multiply: ["$amount", {$cond: [{$eq:["$type", true]}, 1, 0]}] }, 0] } },
           TRY_expense: { $sum: { $cond: [{$eq:["$currency_unit", "TRY"]}, { $multiply: ["$amount", {$cond: [{$eq:["$type", false]}, 1, 0]}] }, 0] } },
           
           USD_income: { $sum: { $cond: [{$eq:["$currency_unit", "USD"]}, { $multiply: ["$amount", {$cond: [{$eq:["$type", true]}, 1, 0]}] }, 0] } },
           USD_expense: { $sum: { $cond: [{$eq:["$currency_unit", "USD"]}, { $multiply: ["$amount", {$cond: [{$eq:["$type", false]}, 1, 0]}] }, 0] } },
           
           EUR_income: { $sum: { $cond: [{$eq:["$currency_unit", "EUR"]}, { $multiply: ["$amount", {$cond: [{$eq:["$type", true]}, 1, 0]}] }, 0] } },
           EUR_expense: { $sum: { $cond: [{$eq:["$currency_unit", "EUR"]}, { $multiply: ["$amount", {$cond: [{$eq:["$type", false]}, 1, 0]}] }, 0] } }
        }
      },
      { $addFields: {
          "TRY": { $add: [ "$TRY_income", {$multiply: [ "$TRY_expense", -1 ] } ] } ,
          "USD": { $add: [ "$USD_income", {$multiply: [ "$USD_expense", -1 ] } ] } ,
          "EUR": { $add: [ "$EUR_income", {$multiply: [ "$EUR_expense", -1 ] } ] } ,
        }
      },
      { $project: { _id: 0 } }
   ]
)