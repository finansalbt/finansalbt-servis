const express = require('express')
const path = require('path')
const bodyParser = require('body-parser')
const cors = require('cors')
const mongoose = require('mongoose')
const config = require('./config/database')

// Connect to mongoose
mongoose
    .connect(config.database_uri, { useNewUrlParser: true })
    .then(() => console.log("Connected to MongoDB"))
    .catch(err => console.log(err))

const app = express()

// routes
const userRoute = require('./routes/UserRoute')
const categoryRoute = require('./routes/CategoryRoute')
const transactionRoute = require('./routes/TransactionRoute')

// port
const port = process.env.PORT || 80

// cors middleware
app.use(cors())

// body parser middleware
app.use(bodyParser.json())

// routes
app.use(userRoute.path, userRoute)
app.use(categoryRoute.path, categoryRoute)
app.use(transactionRoute.path, transactionRoute)

// index route
app.get('/', (req, res) => {
    res.send('anasayfa')
})

app.get('/deneme', (req, res) => {
    res.send('anasayfa')
})

// start server
app.listen(port, () => {
    console.log('Server started on port ' + port)
})
