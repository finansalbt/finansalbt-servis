const express = require('express')
const router = express.Router()

const User = require('../models/User')
const Category = require('../models/Category')
const Transaction = require('../models/Transaction')

router.path = '/user'

// get categories of user
router.get('/getcategories/:user_id', (req, res) => {
	let user_id = req.params.user_id
	User.getUserById(user_id, (err, user) => {
		if (err) return res.json({ success: false, msg: err.msg })
		if (user) {
			if (user.categories) {
				Category.getCategoriesOfUser(user.categories, (err, result) => {
					if (err) return res.json({ success: false, msg: err.msg })
					if (result.success) return res.json({ success: true, categories: result.categories })
					else return res.json({ success: false })
				})
			} else {
				return res.json({ success: true, categories: [] })
			}
		}
		else return res.json({ success: false, msg: 300 })
	})
})

// delete category from user
router.delete('/deletecategory', (req, res) => {
	let user_id = req.body.user_id
	let category_id = req.body.category_id

	User.deleteCategoryFromUser(user_id, category_id, (err, result_of_user) => {
		if (err) return res.json({ success: false, msg: err.msg })
		if (result_of_user.success) {
			Category.delete(category_id, (err, result_of_category) => {
				if (err) return res.json({ success: false, msg: err.msg })
				if (result_of_category.success) {
					User.findById(user_id, (err, user) => {
						if (err) return callback({ msg: err.msg }, null)
						if (user) {
							Transaction.deleteTransactionsOfCategory(category_id, user.transactions, (err, result_of_transactions) => {
								if (err) return res.json({ success: false, msg: err.msg })
								if (result_of_transactions.success) {
									User.deleteTransactionsFromUser(user_id, result_of_transactions.deleted_transactions, (err, result_of_transactions) => {
										if (err) return res.json({ success: false, msg: err.msg })
										if (result_of_transactions.success) {
											return res.json({ success: true })
										}
										else return res.json({ success: false, msg: 700 })
									})
								}
								else return res.json({ success: false, msg: 700 })
							})
						}
						else return callback({ success: false, msg: 300 }, null)
					})
				}
				else return res.json({ success: false, msg: 700 })
			})
		}
		else return res.json({ success: result_of_user.success, msg: result_of_user.msg })
	})
})

// get transactions of user
router.get('/gettransactions/:user_id', (req, res) => {
	let user_id = req.params.user_id
	User.getUserById(user_id, (err, user) => {
		if (err) return res.json({ success: false, msg: err.msg })
		if (user) {
			Transaction.getTransactionsOfUser(user.transactions, (err, result) => {
				if (err) return res.json({ success: false, msg: err.msg })
				if (result.success) return res.json({ success: true, transactions: result.transactions })
				else return res.json({ success: false })
			})
		} else return res.json({ success: false, msg: 300 })

	})
})

// delete transaction from user
router.delete('/deletetransaction', (req, res) => {
	let user_id = req.body.user_id
	let transaction_id = req.body.transaction_id

	User.deleteTransactionFromUser(user_id, transaction_id, (err, result_of_user) => {
		if (err) return res.json({ success: false, msg: err.msg })

		if (result_of_user.success) {
			Transaction.delete(transaction_id, (err, result_of_transaction) => {
				if (err) return res.json({ success: false, msg: err.msg })
				else return res.json({ success: result_of_transaction.success, msg: result_of_transaction.msg })
			})
		}
		else return res.json({ success: result_of_user.success, msg: result_of_user.msg })
	})
})

// register
router.post('/register', (req, res) => {
	let user = new User({
		fullname: req.body.fullname,
		email: req.body.email,
		username: req.body.username,
		password: req.body.password
	})

	User.add(user).then(result_of_user => {
		const newCategory = new Category({
			name: "OTHER"
		})
		Category.add(newCategory).then(category_result => {
			User.addCategoryToUser(result_of_user.user.id, category_result.category.id, (err, result_of_category) => {
				if (err) return res.json({ success: false, msg: err.msg })
				else {
					if (result_of_category.success) {
						result_of_user.user.categories.push(newCategory)
						return res.json({ success: result_of_category.success, user: result_of_user.user })
					} else {
						return res.json({ success: result_of_category.success, msg: result_of_category.msg })
					}
				}
			})
		})
	}).catch(err => {
		return res.json({ success: false, msg: err.message })
	})
})


// login (login)
router.post('/login', (req, res) => {
	const username = req.body.username
	const password = req.body.password

	User.getUserByUsername(username, (err, user) => {
		if (err) return res.json({ success: false, msg: err.msg })
		if (!user) {
			return res.json({ success: false, msg: 300 })
		}

		User.comparePassword(password, user.password, (err, isMatch) => {
			if (err) return res.json({ success: false, msg: err.msg })
			if (isMatch) {

				Category.getCategoriesOfUser(user.categories, (err, result_of_categories) => {
					if (err) return res.json({ success: false, msg: err.msg })
					else {
						user.categories = result_of_categories.categories

						Transaction.getTransactionsOfUser(user.transactions, (err, result_of_transactions) => {
							if (err) return res.json({ success: false, msg: err.msg })
							else {
								user.transactions = result_of_transactions.transactions
								user.password = undefined

								return res.json({
									success: true,
									user: user
								})
							}
						})
					}
				})
			} else {
				return res.json({ success: false, msg: 303 })
			}
		})
	})
})


// get money informations
router.get('/getmoneyinfo/:user_id', (req, res) => {
	let user_id = req.params.user_id;

	User.findById(user_id, (err, user) => {
		if (err) return res.json({ success: false, msg: err.msg })
		if (!user) {
			return res.json({ success: false, msg: 300 })
		} else {
			Transaction.getMoneyValues(user.transactions, (err, result_of_money_values) => {
				return res.json({
					success: true,
					money_values: result_of_money_values.money_values
				})
			})
		}
	})
})


// change password
router.patch('/changepassword', (req, res) => {
	let user_id = req.body.user_id;
	let old_password = req.body.old_password;
	let new_password = req.body.new_password;

	User.changePassword(user_id, old_password, new_password, (err, result) => {
		if (err) return res.json({ success: false, msg: err.msg })
		else return res.json(result)
	})
})


// forget password
router.get('/forgetpassword/:email', (req, res) => {
	let email = req.params.email;

	User.forgetPassword(email, (err, result) => {
		if (err) return res.json({ success: false, msg: err.msg })
		else return res.json(result)
	})
})

module.exports = router
