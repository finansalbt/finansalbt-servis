const express = require('express')
const router = express.Router()

const User = require('../models/User')
const Category = require('../models/Category')

router.path = '/category'

// add category
router.post('/add', (req, res) => {
    const user_id = req.body.user_id;

    const category = new Category({
        name: req.body.category.name.toUpperCase()
    })

    let control = true

    User.getUserById(user_id, (err, user) => {
        if (err) return res.json({ success: false, msg: 300 })
        if (user) {
            Category.getCategoriesOfUser(user.categories, (err, result) => {
                if (err) return res.json({ success: false, msg: err.msg })
                if (result.success) {
                    for (let res_category of result.categories) {
                        if (res_category.name === category.name) {
                            control = false
                            break
                        }
                    }
                    if (control) {
                        Category.add(category).then(result_of_category => {
                            if (result_of_category.success) {
                                User.addCategoryToUser(user_id, result_of_category.category.id, (err, result_of_user) => {
                                    if (err) return res.json({ success: false, msg: err.msg })
                                    else return res.json({ success: result_of_user.success, category: result_of_category.category, msg: result_of_user.msg })
                                })
                            }
                        }).catch(err => {
                            return res.json({ success: false, msg: err.msg })
                        })
                    } else {
                        return res.json({ success: false, msg: 500 })
                    }
                }
            })
        }
        else return res.json({ success: false, msg: 300 })
    })
})

module.exports = router
