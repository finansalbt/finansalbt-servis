const express = require('express')
const router = express.Router()

const User = require('../models/User')
const Category = require('../models/Category')
const Transaction = require('../models/Transaction')

router.path = '/transaction'

// add transaction
router.post('/add', (req, res) => {
    const user_id = req.body.user_id;

    const transaction = new Transaction({
        type: req.body.transaction.type,
        amount: req.body.transaction.amount,
        currency_unit: req.body.transaction.currency_unit,
        category_id: req.body.transaction.category_id,
        description: req.body.transaction.description
    })

    User.getUserById(user_id, (err, user) => {
        if (err) return res.json({ success: false, msg: err.message })
        if (user) {
            Transaction.add(transaction).then(result_of_transaction => {
                if (result_of_transaction.success) {
                    User.addTransactionToUser(user_id, result_of_transaction.transaction.id, (err, result_of_user) => {
                        if (err) return res.json({ success: false, msg: err.msg })
                        else return res.json({ success: result_of_user.success, transaction: result_of_transaction.transaction })
                    })
                }
                else return res.json({ success: false, msg: result_of_transaction.msg })
            }).catch(err => {
                return res.json({ success: false, msg: err.msg })
            })
        }
        else return res.json({ success: false, msg: 300 })
    })
})

// edit transaction
router.put('/edit', (req, res) => {
    const user_id = req.body.user_id;
    const transaction_id = req.body.transaction_id;

    const transaction = new Transaction({
        _id: transaction_id,
        type: req.body.transaction.type,
        amount: req.body.transaction.amount,
        currency_unit: req.body.transaction.currency_unit,
        category_id: req.body.transaction.category_id,
        description: req.body.transaction.description
    })

    User.getUserById(user_id, (err, user) => {
        if (err) return res.json({ success: false, msg: err.msg })
        if (!user) {
            return res.json({ success: false, msg: 300 })
        }
        else if (user.transactions.indexOf(transaction_id) != -1) {
            Transaction.edit(transaction, (err, result) => {
                if (err) return res.json({ success: false, msg: err.msg })
                else return res.json({ success: result.success })
            })
        }
        else return res.json({ success: false, msg: 900 })
    })
})

router.get('/gettransactions/:user_id/:category_id', (req, res) => {
    const user_id = req.params.user_id;
    const category_id = req.params.category_id;

    User.getUserById(user_id, (err, user) => {
        if (err) return res.json({ success: false, msg: err.msg })
        if (user) {
            Transaction.getTransactionsByCategoryIdAndUserId(user.transactions, category_id, (err, transactions) => {
                if (err) return res.json({ success: false, msg: err.msg })
                if (transactions) return res.json(transactions)
                else return res.json({ success: false })
            })
        } else return res.json({ success: false, msg: 300 })
    })
})

module.exports = router
