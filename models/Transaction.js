const mongoose = require('mongoose')

// transaction schema
const TransactionSchema = mongoose.Schema({
    type: {
        type: Boolean,
        required: true
    },
    amount: {
        type: Number,
        required: true
    },
    currency_unit: {
        type: String,
        required: true
    },
    category_id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    description: {
        type: String,
        required: false
    },
    created_at: {
        type: Number
    }
}, { versionKey: false, timestamps: { createdAt: 'created_at', updatedAt: false } })

const Transaction = module.exports = mongoose.model('Transaction', TransactionSchema)

module.exports.getTransactionById = function (id, callback) {
    Transaction.findById(id, callback)
}

module.exports.add = async function (transaction) {
    if (transaction.amount <= 0) {
        return { success: false, msg: 903 }
    }
    else {
        await transaction.save()
        return { success: true, transaction: transaction }
    }
}

module.exports.edit = function (transaction, callback) {
    let result;
    Transaction.updateOne(
        { _id: transaction._id },
        { $set: transaction },
        (err, raw) => {
            if (err) return callback({ success: false, msg: err.msg }, null)
            if (!raw.ok) return callback({ success: false, msg: 700 }, null)
            if (raw.n == 0) {
                return callback({ success: false, msg: 900 }, null)
            } else if (raw.nModified > 0) {
                return callback(null, { success: true })
            } else {
                return callback({ success: false, msg: 700 }, null)
            }
        }
    )
}

module.exports.delete = function (id, callback) {
    Transaction.findByIdAndDelete(
        id,
        (err, res) => {
            if (err) return callback({ msg: err.msg }, null)
            if (res._id) return callback(null, { success: true })
            else return callback({ msg: false }, null)
        }
    )
}

module.exports.deleteTransactionsOfCategory = function (category_id, array_of_transaction, callback) {
    Transaction.find(
        { _id: { $in: array_of_transaction }, category_id: category_id },
        { _id: 1 },
        (err, found_transactions) => {
            if (err) return callback({ success: false, msg: err.msg }, null)
            else {
                deleted_transactions = found_transactions.map(x => x._id);

                Transaction.deleteMany(
                    { _id: { $in: found_transactions } },
                    (err, result) => {
                        if (err) return callback({ success: false, msg: err.msg }, null)
                        else {
                            return callback(null, { success: true, deleted_transactions: deleted_transactions })
                        }
                    }
                )
            }
        }
    )


}

module.exports.getTransactionsOfUser = function (transaction_id_array, callback) {
    Transaction.find({ _id: { $in: transaction_id_array } }, null, { sort: { created_at: -1 } }, (err, res) => {
        if (err) return callback({ success: false, msg: err.msg }, null)
        if (res) return callback(null, { success: true, transactions: res })
        else return callback(null, { success: false })
    })
}

module.exports.getTransactionsByCategoryIdAndUserId = function (transaction_id_array, category_id, callback) {
    Transaction.find({ _id: { $in: transaction_id_array }, category_id: category_id }, null, { sort: { created_at: -1 } }, (err, res) => {
        if (err) return callback({ success: false, msg: err.msg }, null)
        if (res) return callback(null, { success: true, transactions: res })
        else return callback(null, { success: false })
    })
}

module.exports.getMoneyValues = function (transaction_id_array, callback) {

    Transaction.aggregate(
        [
            { $match: { _id: { $in: transaction_id_array } } },
            {
                $group: {
                    _id: null,

                    TRY_income: { $sum: { $cond: [{ $eq: ["$currency_unit", "TRY"] }, { $multiply: ["$amount", { $cond: [{ $eq: ["$type", true] }, 1, 0] }] }, 0] } },
                    TRY_expense: { $sum: { $cond: [{ $eq: ["$currency_unit", "TRY"] }, { $multiply: ["$amount", { $cond: [{ $eq: ["$type", false] }, 1, 0] }] }, 0] } },

                    USD_income: { $sum: { $cond: [{ $eq: ["$currency_unit", "USD"] }, { $multiply: ["$amount", { $cond: [{ $eq: ["$type", true] }, 1, 0] }] }, 0] } },
                    USD_expense: { $sum: { $cond: [{ $eq: ["$currency_unit", "USD"] }, { $multiply: ["$amount", { $cond: [{ $eq: ["$type", false] }, 1, 0] }] }, 0] } },

                    EUR_income: { $sum: { $cond: [{ $eq: ["$currency_unit", "EUR"] }, { $multiply: ["$amount", { $cond: [{ $eq: ["$type", true] }, 1, 0] }] }, 0] } },
                    EUR_expense: { $sum: { $cond: [{ $eq: ["$currency_unit", "EUR"] }, { $multiply: ["$amount", { $cond: [{ $eq: ["$type", false] }, 1, 0] }] }, 0] } }
                }
            },
            {
                $addFields: {
                    "TRY": { $add: ["$TRY_income", { $multiply: ["$TRY_expense", -1] }] },
                    "USD": { $add: ["$USD_income", { $multiply: ["$USD_expense", -1] }] },
                    "EUR": { $add: ["$EUR_income", { $multiply: ["$EUR_expense", -1] }] },
                }
            },
            { $project: { _id: 0 } }
        ]
        , (err, res) => {
            if (err) return callback({ success: false, msg: err.msg }, null)
            if (res) return callback(null, { success: true, money_values: res })
            else return callback(null, { success: false })
        }
    )
}
