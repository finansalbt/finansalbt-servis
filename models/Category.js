const mongoose = require('mongoose')

// category schema
const CategorySchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    }
}, { versionKey: false })

const Category = module.exports = mongoose.model('Category', CategorySchema)

module.exports.add = async function (newCategory) {
    newCategory.name = newCategory.name.toUpperCase()
    await newCategory.save()
    return { success: true, category: newCategory }
}

module.exports.edit = function (id, category, callback) {
    Category.updateOne(
        { _id: id },
        { name: category.name },
        (err, raw) => {
            if (err) return callback({ msg: err.msg }, null)
            if (!raw.ok) return callback({ msg: 700 }, null)
            if (raw.n == 0) return callback({ msg: 501 }, null)
            else if (raw.nModified > 0) return callback(null, { success: true })
            else return callback(null, { success: false })
        }
    )
}

module.exports.delete = function (id, callback) {
    Category.findByIdAndDelete(
        id,
        (err, res) => {
            if (err) return callback({ msg: err.msg }, null)
            if (res._id) return callback(null, { success: true })
            else return callback({ msg: false }, null)
        }
    )
}

module.exports.getCategoriesOfUser = function (category_id_array, callback) {
    Category.find({ _id: { $in: category_id_array } }, (err, res) => {
        if (err) return callback({ success: false, msg: err.msg }, null)
        if (res) return callback(null, { success: true, categories: res })
        else return callback(null, { success: false })
    })
}
